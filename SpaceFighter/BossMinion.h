#pragma once

#include "EnemyShip.h"

class BossMinion : public EnemyShip
{

public:

	BossMinion();

	virtual ~BossMinion() { };

	virtual void SetTexture(Texture* pTexture) { m_pTexture = pTexture; }

	virtual void Update(const GameTime* pGameTime);

	virtual void Draw(SpriteBatch* pSpriteBatch);


private:

	Texture* m_pTexture = nullptr;

	double aes_activationSeconds;

};
