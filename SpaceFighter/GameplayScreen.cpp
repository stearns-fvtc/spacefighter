
#include "GameplayScreen.h"
#include "Level.h"
#include "Level01.h"
#include "Level02.h"
#include "LevelBoss.h"

GameplayScreen::GameplayScreen(const int levelIndex)
{
	
	LoadLevel(levelIndex);

	SetTransitionInTime(1.0f);
	SetTransitionOutTime(0.5f);

	Show();
}

void GameplayScreen::LoadContent(ResourceManager *pResourceManager)
{
	m_pLevel->LoadContent(pResourceManager);
	m_pResourceManager = pResourceManager;
	
}
void GameplayScreen::LoadLevel(const int levelIndex)
{
	if (m_pLevel) delete m_pLevel; // Delete Level if one exists
	switch (levelIndex)
	{
	case 0: m_pLevel = new Level01(); break;
	case 1: m_pLevel = new Level02(); break;
	case 2: m_pLevel = new LevelBoss(); break;
	}

	m_pLevel->SetGameplayScreen(this);

	if (m_pResourceManager) m_pLevel->LoadContent(m_pResourceManager);
}
void GameplayScreen::HandleInput(const InputState *pInput)
{
	m_pLevel->HandleInput(pInput);
}

void GameplayScreen::Update(const GameTime *pGameTime)
{
	m_pLevel->Update(pGameTime);
}

void GameplayScreen::Draw(SpriteBatch *pSpriteBatch)
{
	pSpriteBatch->Begin();

	m_pLevel->Draw(pSpriteBatch);

	pSpriteBatch->End();
}
