#include "BossMinion1.h"


BossMinion1::BossMinion1()
{
	SetSpeed(30);
	SetMaxHitPoints(1);
	SetCollisionRadius(25);
	m_scoreValue = 10000;
}


void BossMinion1::Update(const GameTime* pGameTime)
{
	if (IsActive())
	{
		aes_activationSeconds += pGameTime->GetTimeElapsed();

		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
		x *= GetSpeed() * pGameTime->GetTimeElapsed() * .01f;
		TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed());

		if (aes_activationSeconds > 8)
		{
			TranslatePosition(x + 1.4f, GetSpeed() * pGameTime->GetTimeElapsed() * 6);
		}

		if (!IsOnScreen()) Deactivate();
	}

	EnemyShip::Update(pGameTime);
}


void BossMinion1::Draw(SpriteBatch* pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter(), Vector2::ONE, Math::PI, 1);
	}
}