#include "Boss.h"

Boss::Boss()
{
	SetSpeed(20);
	SetMaxHitPoints(50);
	SetCollisionRadius(150);
	m_scoreValue = 100000;
}


void Boss::Update(const GameTime* pGameTime)
{
	if (IsActive())
	{
		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
		x *= GetSpeed() * pGameTime->GetTimeElapsed() * .1f;
		TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed());

		aes_activationSeconds += pGameTime->GetTimeElapsed();
		if (aes_activationSeconds > 6 && aes_activationSeconds < 20)
		{
			float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
			x *= GetSpeed() * pGameTime->GetTimeElapsed() * 10.5f;
			float y = cos(pGameTime->GetTotalTime() * Math::PI + GetIndex());
			y *= GetSpeed() * pGameTime->GetTimeElapsed() * 4.2f;
			TranslatePosition(x, y * (GetSpeed() * pGameTime->GetTimeElapsed()) * 10);
			TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed() / 2);
		}


		if (aes_activationSeconds > 20 && aes_activationSeconds < 24) {
			float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
			x *= GetSpeed() * pGameTime->GetTimeElapsed() * 2.4f;
			float y = cos(pGameTime->GetTotalTime() * Math::PI + GetIndex());
			y *= GetSpeed() * pGameTime->GetTimeElapsed() * 1.4;

			TranslatePosition(x, y * (GetSpeed() * pGameTime->GetTimeElapsed()) - 2);

		}
		if (aes_activationSeconds > 24 && aes_activationSeconds < 30)
		{
			float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
			x *= GetSpeed() * pGameTime->GetTimeElapsed() * 10.5f;
			TranslatePosition(x * 3, GetSpeed() * pGameTime->GetTimeElapsed() * 6);
		}
		if (aes_activationSeconds > 30 && aes_activationSeconds < 32)
		{
			float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
			x *= GetSpeed() * pGameTime->GetTimeElapsed() * 2.4f;
			float y = cos(pGameTime->GetTotalTime() * Math::PI + GetIndex());
			y *= GetSpeed() * pGameTime->GetTimeElapsed() * 1.4;

			TranslatePosition(x, y * (GetSpeed() * pGameTime->GetTimeElapsed()) - 7);
		}
		if (aes_activationSeconds > 32 && aes_activationSeconds < 36)
		{
			float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
			x *= GetSpeed() * pGameTime->GetTimeElapsed() * 7.5f;
			float y = cos(pGameTime->GetTotalTime() * Math::PI + GetIndex());
			y *= GetSpeed() * pGameTime->GetTimeElapsed() * 4.2f;
			TranslatePosition(x, y * (GetSpeed() * pGameTime->GetTimeElapsed()) * 10);
			TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed() / 2);
		}
		if (aes_activationSeconds > 36 && aes_activationSeconds < 40)
		{
			float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
			x *= GetSpeed() * pGameTime->GetTimeElapsed() * 10.5f;
			TranslatePosition(x * 3, GetSpeed() * pGameTime->GetTimeElapsed() * 2);
		}
		if (aes_activationSeconds > 40)
		{
			float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
			x *= GetSpeed() * pGameTime->GetTimeElapsed() * 7.5f;
			float y = cos(pGameTime->GetTotalTime() * Math::PI + GetIndex());
			y *= GetSpeed() * pGameTime->GetTimeElapsed() * 4.2f;
			TranslatePosition(x, y * (GetSpeed() * pGameTime->GetTimeElapsed()) * 10);
			TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed() / 2);
		}




		if (!IsOnScreen()) Deactivate();
	}

	EnemyShip::Update(pGameTime);
}


void Boss::Draw(SpriteBatch* pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter(), Vector2::ONE, Math::PI, 1);
	}
}