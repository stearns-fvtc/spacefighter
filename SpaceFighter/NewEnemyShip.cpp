#include "NewEnemyShip.h"

NewEnemyShip::NewEnemyShip()
{
	SetSpeed(30);
	SetMaxHitPoints(4);
	SetCollisionRadius(25);
	m_scoreValue = 7500;
}

void NewEnemyShip::Update(const GameTime* pGameTime)
{
	if (IsActive())
	{
		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
		x *= GetSpeed() * pGameTime->GetTimeElapsed() * 1.4f;
		TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed());

		aes_activationSeconds += pGameTime->GetTimeElapsed();
		if (aes_activationSeconds > 2)
		{
			float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
			x *= GetSpeed() * pGameTime->GetTimeElapsed() * 1.4f;
			float y = cos(pGameTime->GetTotalTime() * Math::PI + GetIndex());
			y *= GetSpeed() * pGameTime->GetTimeElapsed() * 2.8f;
			TranslatePosition(x, y * (GetSpeed() * pGameTime->GetTimeElapsed()) * 4);
			TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed() / 2);
		}


		if (!IsOnScreen()) Deactivate();
	}

	EnemyShip::Update(pGameTime);
}

void NewEnemyShip::Draw(SpriteBatch* pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(a_pTexture, GetPosition(), Color::White, a_pTexture->GetCenter(), Vector2::ONE, Math::PI, 1);
	}
}