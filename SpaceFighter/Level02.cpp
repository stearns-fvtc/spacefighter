#include "Level02.h"
#include "BioEnemyShip.h"
#include "NewEnemyShip.h"
#include "Blaster02.h"
#include "GameplayScreen.h"

Level02::Level02()
{
	Blaster02* pBlaster = new Blaster02(true);
	pBlaster->SetProjectilePool(&m_projectiles);
	m_pPlayerShip->AttachWeapon(pBlaster, Vector2::UNIT_Y * -20);
}

void Level02::LoadContent(ResourceManager* pResourceManager)
{
	// Setup enemy ships
	Texture* pTexture = pResourceManager->Load<Texture>("Textures\\BioEnemyShip.png");
	Texture* pTexture2 = pResourceManager->Load<Texture>("Textures\\NewEnemyShip.png");

	const int COUNT = 21;
	const int COUNT2 = 11;

	double xPositions[COUNT] =
	{
		0.25, 0.2, 0.3,
		0.75, 0.8, 0.7,
		0.3, 0.25, 0.35, 0.2, 0.4,
		0.7, 0.75, 0.65, 0.8, 0.6,
		0.5, 0.4, 0.6, 0.45, 0.55
	};

	double xPositions2[COUNT2] =
	{
		0.45, 0.2, 0.6,
		0.55, 0.8, 0.9,
		0.3, 0.20, 0.6, 0.2
	};

	double delays[COUNT] =
	{
		0.1, 0.15, 0.35,
		3.0, 0.35, 0.25,
		3.25, 0.45, 0.25, 0.25, 0.7,
		5.5, 0.25, 0.25, 0.25, 0.65,
		3.5, 0.2, 0.3, 0.45, 0.3
	};

	double delays2[COUNT2] =
	{
		0.0, 0.25, 0.25,
		3.0, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25
	};

	float delay = 1.5; // start delay
	Vector2 position;
	Vector2 position2;

	for (int i = 0, j = 0; i < COUNT && j < COUNT2; i++, j++)
	{
		delay += delays[i];
		delay += delays2[j];

		position.Set(xPositions[i] * Game::GetScreenWidth(), -pTexture->GetCenter().Y);

		BioEnemyShip* pEnemy = new BioEnemyShip();
		pEnemy->SetTexture(pTexture);
		pEnemy->SetCurrentLevel(this);
		pEnemy->Initialize(position, (float)delay);
		AddGameObject(pEnemy);

		position2.Set(xPositions2[j] * Game::GetScreenWidth(), -pTexture2->GetCenter().Y);

		NewEnemyShip* pEnemy2 = new NewEnemyShip();
		pEnemy2->SetTexture(pTexture2);
		pEnemy2->SetCurrentLevel(this);
		pEnemy2->Initialize(position2, (float)delay);
		AddGameObject(pEnemy2);
	}

	Level::LoadContent(pResourceManager);
}
void Level02::Update(const GameTime* pGameTime)
{

	if (m_pPlayerShip->IsActive() == true && pGameTime->GetTotalTime() > 67)
	{
		GetGameplayScreen()->LoadLevel(2);
		return;
	}

	Level::Update(pGameTime);
};