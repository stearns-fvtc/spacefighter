#pragma once

#include "EnemyShip.h"

class NewEnemyShip : public EnemyShip
{
public:
	NewEnemyShip();

	virtual ~NewEnemyShip() { };

	virtual void SetTexture(Texture* pTexture) { a_pTexture = pTexture; }

	virtual void Update(const GameTime* pGameTime);

	virtual void Draw(SpriteBatch* pSpriteBatch);

private:
	Texture* a_pTexture = nullptr;

	double aes_activationSeconds;
};