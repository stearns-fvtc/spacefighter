#pragma once
#include <string>
#include "DataAccess.h"

class Score
{
public:
	static std::string GetScoreString();

	static int GetScore();

	static void IncreaseScoreBy(int value);

	static void SaveHiScore();

	static bool IsHiScore();

	static string GetHiScoreString();

private:
	static unsigned int m_score;

	static std::string m_scoreDisplay;
};

