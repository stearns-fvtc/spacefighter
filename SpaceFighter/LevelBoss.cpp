#include "LevelBoss.h"
#include "Boss.h"
#include "BossMinion.h"
#include "BossMinion1.h"
#include "Blaster02.h"
#include "GameplayScreen.h"

LevelBoss::LevelBoss()
{
	Blaster02* pBlaster = new Blaster02(true);
	pBlaster->SetProjectilePool(&m_projectiles);
	m_pPlayerShip->AttachWeapon(pBlaster, Vector2::UNIT_Y * -20);
}

void LevelBoss::LoadContent(ResourceManager* pResourceManager)
{
	// Setup enemy ships
	Texture* pTexture = pResourceManager->Load<Texture>("Textures\\Boss.png");
	Texture* pTexture2 = pResourceManager->Load<Texture>("Textures\\BossMinion.png");


	const int COUNT = 10;
	const int COUNT2 = 10;
	double bPosition = 0.5;
	double xPositions[COUNT] =
	{
		0.45, 0.41, 0.37,
		0.33, 0.29,
		0.45, 0.41, 0.37,
		0.33, 0.29,
	};
	double xPositions2[COUNT2] =
	{
		 0.55, 0.59, 0.63,
		 0.67, 0.71,
		 0.55, 0.59, 0.63,
		 0.67, 0.71,
	};
	double delays[COUNT] =
	{
		0.5, 0.15, 0.15, 0.15, 0.15,
		10, 0.15, 0.15, 0.15, 0.15
	};
	double delays2[COUNT2] =
	{
		0.5, 0.15, 0.15, 0.15, 0.15,
		10, 0.15, 0.15, 0.15, 0.15
	};


	float delay = 1.5; // start delay
	Vector2 position;
	Vector2 position2;
	Vector2 position3;

	position3.Set(bPosition * Game::GetScreenWidth(), -pTexture->GetCenter().Y);

	Boss* pBoss = new Boss();
	pBoss->SetTexture(pTexture);
	pBoss->SetCurrentLevel(this);
	pBoss->Initialize(position3, (float)delay);
	AddGameObject(pBoss);

	for (int i = 0, j = 0; i < COUNT&& j < COUNT2; i++, j++)
	{
		delay += delays[i];
		delay += delays2[j];

		position.Set(xPositions[i] * Game::GetScreenWidth(), -pTexture2->GetCenter().Y);

		BossMinion* pEnemy = new BossMinion();
		pEnemy->SetTexture(pTexture2);
		pEnemy->SetCurrentLevel(this);
		pEnemy->Initialize(position, (float)delay);
		AddGameObject(pEnemy);

		position2.Set(xPositions2[j]* Game::GetScreenWidth(), -pTexture2->GetCenter().Y);

		BossMinion1* pEnemy1 = new BossMinion1();
		pEnemy1->SetTexture(pTexture2);
		pEnemy1->SetCurrentLevel(this);
		pEnemy1->Initialize(position2, (float)delay);
		AddGameObject(pEnemy1);
	}

	




	Level::LoadContent(pResourceManager);
}