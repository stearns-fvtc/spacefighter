#include "Score.h"

unsigned int Score::m_score = 0;

std::string Score::m_scoreDisplay = "000000000";

// Returns the formatted score string.
std::string Score::GetScoreString()
{	
	// Because the display score is structured like an arcade's score, we first have to determine the total real score length. (m_score)
	// Then, create a new score by determing the number of digits left after the real score digits take up the rest of the spots. (m_scoreDisplay.substr(0, 9 - scoreLen))
	// And add the real score into the "spots" left over! (+ std::to_string(m_score))

	int scoreLen = std::to_string(m_score).length();
	m_scoreDisplay = m_scoreDisplay.substr(0, 9 - scoreLen) + std::to_string(m_score);

	return m_scoreDisplay;
}

// Returns the actual score value.
int Score::GetScore()
{
	return m_score;
}

// Increases the score by the specified value.
void Score::IncreaseScoreBy(int value)
{
	m_score += value;
}

// Saves score and returns true if it is a high score
void Score::SaveHiScore()
{
	if (IsHiScore())
	{
		DataAccess dat;
		dat.SaveToFile("hiscore.txt", m_scoreDisplay);
	}
}

// Checks if the current score is a high score
bool Score::IsHiScore()
{
	if (m_score > stoi(GetHiScoreString())) return true;
	else return false;
}

string Score::GetHiScoreString()
{
	DataAccess dat;
	return dat.LoadFromFile("hiscore.txt");
}


