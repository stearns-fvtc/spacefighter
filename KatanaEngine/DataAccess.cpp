#include "DataAccess.h"

using namespace std;

// Load from a .txt file
string DataAccess::LoadFromFile(string filepath)
{
	fstream file;
	file.open(filepath);

	if (file.is_open())
	{
		string line;
		string results;
		while (getline(file, line))
		{
			results += line + "\n";
		}

		file.close();
		return results;
	}
	else { throw exception("File could not be loaded."); }
}

// Save to a .txt file
void DataAccess::SaveToFile(string filepath, string content)
{
	ofstream file;
	file.open(filepath);

	if (file.is_open())
	{
		file << content;
		file.close();
	}
	else { throw exception("File could not be saved."); }
}