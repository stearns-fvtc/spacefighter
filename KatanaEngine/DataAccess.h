#pragma once
#include <fstream>
#include <iostream>
#include <string>

using namespace std;

class DataAccess
{
public:
	void SaveToFile(string filepath, string content);
	string LoadFromFile(string filepath);
};

